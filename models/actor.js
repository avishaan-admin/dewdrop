var mongoose = require('mongoose');

/*
|-------------------------------------------------------------
| Actor Schema
|-------------------------------------------------------------
*/

var actorSchema = new mongoose.Schema({
  facebookId: {type: String},
  trusted: [{
    type: mongoose.Schema.Types.ObjectId, ref: 'Actor'
  }]
});